[![Deploy Vault Server at Hetzner](https://gitlab.com/ackersonde/hetzner_vault/badges/main/pipeline.svg)](https://gitlab.com/ackersonde/hetzner_vault/-/commits/main)

# Description
Deploys a [userdata config'd](https://gitlab.com/ackersonde/hetzner_home/-/raw/main/scripts/raw_ubuntu_userdata.sh) Hashicorp Vault server to the Hetzner cloud while destroying the previous instance.
